<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="bodyWrapper">
    <div class="cap">
        <div class="wrapper">
            <div class="logo">
                <h1><a href="/" alt="Региональный портал медицинских услуг"></a></h1>
            </div>
            <div class="name">
                <h1><a href="/">Regional tibbiy xizmatlar portali</a></h1>
            </div>
            <div class="region">
                <ul class="closed">
                    <li class="selected">
                        <a href="http://emedic.uz">Xorazm viloyati</a>
                    </li>
                </ul>
            </div>
            <div class="person">
                <?php if(!\Yii::$app->user->isGuest){ ?>
                <p>
                    <a href="/user/profile" class="enter"><span></span>Shaxsiy kabinet</a>
<!--                    <a href="/user/cards" class="folder"><span></span>Моя картотека</a> -->
                    <a href="<?=\Yii::$app->urlManager->createUrl(['main/logout'])?>" data-method="post" class="exit">Выход</a>
                </p>
                <?php } else {?>
                    <p><a href="<?=\Yii::$app->urlManager->createUrl(['main/login'])?>" class="enter "><span></span>Вход</a>
                        <a href="/user/register" class="register"><span></span>Регистрация</a>
                        <a href="#" class="demo" onclick="return false;"><span></span>Демо-режим</a>
                    </p>

                    <div class="popup enterBox">
                        <span class="close"></span>
                        <form id="login-form" method="post">
                            <p><input id="login_email" placeholder="Логин или Электронная почта" name="username" value="" type="text">
                            </p>
                            <p class="password-field"><input placeholder="Пароль" name="password" type="password">
                                <label style="margin: 0;height: 0;display: block;">&nbsp;</label>
                            </p>
                            <p><input id="forgot" name="remember" type="checkbox"><label for="forgot">Запомнить меня</label><a href="#" class="forgot_pass_link">Забыли пароль?</a></p>
                            <p><input value="Вход" name="login-submit" type="submit"></p>
                            <p><a href="/login/esia">Войти через портал Госуслуг РФ (ЕСИА)</a> <span class="info" title="Авторизация через портал Госуслуг РФ открывает доступ ко всем услугам Медицинского портала, в том числе к Медицинской карте."></span></p>
                        </form>
                    </div>

                    <div class="demoBox tooltips">
                        <div>
                            <p>В демо-режиме можно ознакомиться со всеми возможностями портала без регистрации. Изменения сохраняться не будут.</p>
                            <p>&nbsp;</p>
                            <p><a href="/user/demo_login" class="button grey">Включить демо-режим</a></p>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
    <?php if(\Yii::$app->controller->action->id == "index" && \Yii::$app->controller->id == "main" ) { ?>
    <div class="top-slideshow">
        <div class="wrapper slick-initialized slick-slider">
            <div tabindex="0" aria-live="polite" class="slick-list draggable">
                <div style="opacity: 1; width: 5544px; transform: translate(-1848px, 0px);" class="slick-track">
                    <div style="width: 924px;" aria-hidden="true" data-slick-index="-1" class="top-slider slick-slide slick-cloned">
                        <div class="slider-img">
                            <img src="/img/slides/slide6.png">
                        </div>
                        <div class="slider-descr">
                            <h2>Поиск участка прикрепления</h2>
                            <p>
                                Поиск участка прикрепления по адресу и список поликлиник с участками.
                            </p>
                            <a href="/service/regions" class="slider-btn">Найти</a>
                        </div>
                    </div>
                    <div style="width: 924px;" aria-hidden="true" data-slick-index="0" class="top-slider slick-slide">
                        <div class="slider-img">
                            <img src="/img/slides/slide1.png">
                        </div>
                        <div class="slider-descr">
                            <h2>Запись на приём к врачу</h2>
                            <p>
                                Записаться на приём к врачу по месту жительства быстро, просто и удобно.
                                <a href="/user/register">Зарегистрируйтесь</a> на сайте или <a href="#" onclick="$('.enter').click();return false;">авторизуйтесь</a> для возможности записаться.</p>
                            <a href="/service/record" class="slider-btn">Записаться</a>
                        </div>
                    </div>
                    <div style="width: 924px;" aria-hidden="false" data-slick-index="1" class="top-slider slick-slide slick-active">
                        <div class="slider-img">
                            <img src="/img/slides/slide5.png">
                        </div>
                        <div class="slider-descr">
                            <h2>Расписание работы врачей</h2>
                            <p>
                                Информация о расписании работы врачей медицинских организаций.
                            </p>
                            <a href="/service/schedule" class="slider-btn">Открыть</a>
                        </div>
                    </div>
                    <div style="width: 924px;" aria-hidden="true" data-slick-index="2" class="top-slider slick-slide">
                        <div class="slider-img">
                            <img src="/img/slides/slide2.png">
                        </div>
                        <div class="slider-descr">
                            <h2>Медицинские организации в регионе</h2>
                            <p>
                                Доступ к информации о медицинских организациях региона.
                            </p>
                            <a href="/service/hospitals" class="slider-btn">Открыть</a>
                        </div>
                    </div>
                    <div style="width: 924px;" aria-hidden="true" data-slick-index="3" class="top-slider slick-slide">
                        <div class="slider-img">
                            <img src="/img/slides/slide6.png">
                        </div>
                        <div class="slider-descr">
                            <h2>Поиск участка прикрепления</h2>
                            <p>
                                Поиск участка прикрепления по адресу и список поликлиник с участками.
                            </p>
                            <a href="/service/regions" class="slider-btn">Найти</a>
                        </div>
                    </div>
                    <div style="width: 924px;" aria-hidden="true" data-slick-index="4" class="top-slider slick-slide slick-cloned">
                        <div class="slider-img">
                            <img src="/img/slides/slide1.png">
                        </div>
                        <div class="slider-descr">
                            <h2>Запись на приём к врачу</h2>
                            <p>
                                Записаться на приём к врачу по месту жительства быстро, просто и удобно.
                                <a href="/user/register">Зарегистрируйтесь</a> на сайте или <a href="#" onclick="$('.enter').click();return false;">авторизуйтесь</a> для возможности записаться. 		</p>
                            <a href="/service/record" class="slider-btn">Записаться</a>
                        </div>
                    </div>
                </div>
            </div>
            <button style="display: block;" type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>
            <button style="display: block;" type="button" data-role="none" class="slick-next" aria-label="next">Next</button>
            <ul style="display: block;" class="slick-dots">
                <li aria-hidden="true" class="">
                    <button type="button" data-role="none">1</button>
                </li>
                <li class="slick-active" aria-hidden="false">
                    <button type="button" data-role="none">2</button>
                </li>
                <li aria-hidden="true">
                    <button type="button" data-role="none">3</button>
                </li>
                <li aria-hidden="true">
                    <button type="button" data-role="none">4</button>
                </li>
            </ul>
        </div>
    </div>
    <?php } ?>
    <div class="path">
        <div class="wrapper">
            <span><a href="/">Главная</a></span>
            <!--            <span>Регистрация</span> -->
            <span><?php echo (isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'][0] : ""); ?></span>
        </div>
    </div>
    <div class="content">
        <?= $content ?>
    </div>
    <div class="push"></div>
</div>

<div class="footer">
    <div class="pants">
        <div class="wrapper">
            <div class="applet">
            </div>
            <ul>
                <li><a href="<?=\Yii::$app->urlManager->createUrl(['main/about'])?>">Biz haqimizda</a></li>
<!--                <li><a href="/main/news">Yangiliklar</a></li> -->
                <li><a href="<?=\Yii::$app->urlManager->createUrl(['main/contact'])?>">Qayta aloqa</a></li>
            </ul>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
