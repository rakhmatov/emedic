<?php
/* @var $this yii\web\View */
?>
<div class="wrapper">
    <h1>Profil</h1>
    <form method="post" id="add-box" action="" accept-charset="utf-8">
        <h5><span>Shaxsiy ma'lumotlar</span></h5>
        <div class="cols">
            <div class="col50">
                <p><label>Familiya *</label><input class="profile" disabled="" value="<?=\Yii::$app->user->identity->family?>" type="text"></p>
                <p><label>Ism *</label><input class="profile" disabled="" value="<?=\Yii::$app->user->identity->name?>" type="text"></p>
                <p><label>Otasining ismi *</label><input class="profile" disabled="" value="<?=\Yii::$app->user->identity->father_name?>" type="text"></p>
                <p><label>Tu'gilgan sana *</label>
                        <select aria-hidden="true" tabindex="-1" class="w33 profile select2-hidden-accessible" disabled="">
                                <option><?=\Yii::$app->user->identity->birth_day?></option>
                        </select>
                    <select aria-hidden="true" tabindex="-1" class="w33 profile select2-hidden-accessible" disabled="">
                        <option><?=\Yii::$app->user->identity->birth_month?></option>
                    </select>
                    <select aria-hidden="true" tabindex="-1" class="w33 profile select2-hidden-accessible" disabled="">
                        <option><?=\Yii::$app->user->identity->birth_year?></option>
                    </select>
                </p>
            </div>
            <div class="col50">
                &nbsp;
            </div>
        </div>
        <h5><span>Фактический адрес и полис</span></h5>
        <div class="cols">
            <div class="col50">
                <p>
                        <label>Tuman *</label>
                        <select aria-hidden="true" tabindex="-1" class="w100 select2-hidden-accessible" name="territory-id" id="territory-id">
                                <option selected="" value="0">Tanlanmagan</option>
                                <?php foreach($regions as $region){ ?>
                                        <option value="<?=$region->id?>"><?=$region->name?></option>
                                <? } ?>
                        </select>
                    <span class="error"></span>
                </p>
                <p><label>Address</label><input name="address" value="<?=\Yii::$app->user->identity->address?>" type="text">
                    <span class="error"></span>
                </p>
                <p>
                    <label>Телефон</label><input class="small" value="<?=\Yii::$app->user->identity->phone?>" id="mobile_phone" name="mobile_phone" type="text">
                    <input id="mobile_phone_old" value="" type="hidden">
                    <input id="activation_status_old" value="" type="hidden">
                    <span class="info" title="SMS-habar</br> uchun ma'lumotlarni kiriting"></span><br>
                </p><div id="phone_status">
                </div>
                <p></p>
                <p><label>Полис(INN) *</label><input name="ins-ser" class="small_ser profile" style="width:50px;" value="ИТ" placeholder="Серия" disabled="disabled" type="text"> <input name="ins-num" class="small2 profile" value="123654" placeholder="Номер" disabled="disabled" type="text"><br>
                </p>
            </div>
        </div>
        <h5><span>Identifikator ma'lumotlar</span></h5>
        <div class="cols">
            <div class="col50">
                <p><label>Elektron manzil *</label><input name="email" placeholder="Elektron manzil" value="<?=\Yii::$app->user->identity->email?>" disabled="disabled" class="profile" type="text">
                    <span class="error"></span>
                </p>
                <label>&nbsp;</label><span><a href="#" id="change_pass_link">Parolni o'zgartirish</a></span><br><br><br>
                <div class="hidden" id="change_pass_block">
                    <p><label>Пароль *</label><input value="" id="password-input" name="password" type="password"><br>
                        <label>&nbsp;</label>
                        <span class="text-inlinetooltip">Kamida 6 ta simvol.</span>
                        <span class="error"></span>
                    </p>
                    <p><label>Parolni tasdiqlang *</label><input value="" id="password-confirm-input" name="password_confirm" type="password">
                        <span class="error" rel="password-confirm-input"></span>
                    </p>
                </div>
            </div>
            <div class="col50">
                <p>&nbsp;</p>
            </div>
        </div>
        <h5><span>Bildirgi</span></h5>
        <div class="cols">
            <div class="col50">
                <p>
                    <label>&nbsp;</label>
                    Получать оповещения из региональной медицинской системы<br>
                    <label>&nbsp;</label><input id="notify_email" name="notify_options[notify_email]" value="1" class="checkBox" checked="" type="checkbox"><label class="agree" for="notify_email">По электронной почте</label><br><br>
                    <label>&nbsp;</label><input id="notify_sms" name="notify_options[notify_sms]" value="1" class="checkBox" disabled="" type="checkbox"><label class="agree disabled" for="notify_sms">По СМС</label><br><br>
                </p>
            </div>
            <div class="col50">
                <p>Указав эти настройки, вы сможете получать оповещения из региональной медицинской системы, например, напоминания о будущих посещениях, на которые вас записали в больнице.<br>Не влияет на оповещения по записям, сделанным с портала.<br>Для включения оповещений по СМС укажите и потвердите телефон в поле выше.</p>
            </div>
        </div>
        <p><label>&nbsp;</label><input value="Сохранить" type="submit"></p>
    </form>
</div>