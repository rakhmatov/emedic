<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = "Ro'yhatdan o'tish";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper">
    <h1>Ro'yhatdan o'tish</h1>
    <form method="post" id="add-box" action="" accept-charset="utf-8">
        <h5><span>Shaxsiy ma'lumotlar</span></h5>

        <?php if($register->hasErrors()):?>
            <div class="alert alert-danger">
                <?= nl2br(Html::errorSummary($register)) ?>
            </div>
        <?php endif;?>

        <div class="cols">
            <div class="col50">
                <input name="<?=\Yii::$app->request->csrfParam?>" value="<?=\Yii::$app->request->getCsrfToken()?>" type="hidden" />
                <p><label>Familiya *</label><input name="User[family]" class="profile" type="text"></p>
                <p><label>Ism *</label><input name="User[name]" class="profile" value="<?=$register->name?>" type="text"></p>
                <p><label>Otasining ismi *</label><input name="User[father_name]" class="profile" value="<?=$register->father_name?>" type="text"></p>
                <p><label>Tu'gilgan sana *</label>
                    <select name="User[birth_day]" aria-hidden="true" tabindex="-1" class="w33 profile select2-hidden-accessible">
                        <option><?=$register->birth_day?></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                    <select name="User[birth_month]" aria-hidden="true" tabindex="-1" class="w33 profile select2-hidden-accessible">
                        <option><?=$register->birth_month?></option>
                        <option value="1">Yanvar</option>
                        <option value="2">Fevral</option>
                        <option value="3">Mart</option>
                        <option value="4">Aprel</option>
                        <option value="5">May</option>
                    </select>
                    <select name="User[birth_year]" aria-hidden="true" tabindex="-1" class="w33 profile select2-hidden-accessible">
                        <option><?=$register->birth_year?></option>
                        <option value="1992">1992</option>
                        <option value="1993">1993</option>
                        <option value="1994">1994</option>
                        <option value="1995">1995</option>
                        <option value="1996">1996</option>
                    </select>
                </p>
            </div>
            <div class="col50">
                &nbsp;
            </div>
        </div>
        <h5><span>Haqiqiy ma'lumotlar</span></h5>
        <div class="cols">
            <div class="col50">
                <p>
                    <label>Tuman *</label>
                    <select name="User[region]" aria-hidden="true" tabindex="-1" class="w100 select2-hidden-accessible" name="territory-id" id="territory-id">
                        <option selected="" value="0">Tanlanmagan</option>
                        <?php foreach($regions as $region){ ?>
                            <option value="<?=$region->id?>"><?=$region->name?></option>
                        <?php } ?>
                    </select>
                    <span class="error"></span>
                </p>
                <p><label>Address</label><input name="User[address]" name="address" type="text">
                    <span class="error"></span>
                </p>
                <p>
                    <label>Telefon</label><input name="User[phone]" class="profile" value="" id="mobile_phone" name="mobile_phone" type="text">
                    <input id="mobile_phone_old" value="" type="hidden">
                    <input id="activation_status_old" value="" type="hidden">
                    <span class="info" title="SMS-habar</br> uchun ma'lumotlarni kiriting"></span><br>
                </p><div id="phone_status">
                </div>
                <p></p>
                <p><label>STIR (ИНН) *</label><input name="User[inn]" class="profile" value="" placeholder="STIR raqami" type="text"><br>
                </p>
            </div>
        </div>
        <h5><span>Identifikator ma'lumotlar</span></h5>
        <div class="cols">
            <div class="col50">
                <p><label>Elektron manzil *</label><input name="User[email]" placeholder="Elektron manzil" id="email-input" type="text">
                    <span class="error"></span>
                </p>
                <div id="change_pass_block">
                    <p><label>Parol *</label><input name="User[passwd]" id="password-input" type="password"><br>
                        <label>&nbsp;</label>
                        <span class="text-inlinetooltip">Kamida 6 ta simvol.</span>
                        <span class="error"></span>
                    </p>
                    <p><label>Parolni tasdiqlang *</label><input name="User[confirm_password]" id="password-confirm-input" type="password">
                        <span class="error" rel="password-confirm-input"></span>
                    </p>
                </div>
            </div>
            <div class="col50">
                <p>&nbsp;</p>
            </div>
        </div>
        <p><label>&nbsp;</label><input value="Ro'yhatdan o'tish" type="submit"></p>
    </form>
</div>