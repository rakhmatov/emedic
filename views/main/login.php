<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="cols">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal','class' => 'col50'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => [],
            ],
        ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder'=>'Elektron pochta (Email)']) ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Parol']) ?>

<!--        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<label></label><div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?> -->

        <label></label>
        <p><input value="Kirish" name="login-submit" type="submit"></p>

        <?php ActiveForm::end(); ?>
    </div>
</div>
