<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Web server sani so'rovingni bajarish vaqtinda xatolik yuz bardi.
    </p>
    <p>
        Bu haqida bizara habar bar. Rahmat.
    </p>
    <a class="button" href="<?=\Yii::$app->urlManager->createUrl(['main/index'])?>">Вернуться на главную страницу</a>
</div>
