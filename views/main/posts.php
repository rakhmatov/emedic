<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'Yangilikar';
?>
<div class="main-index">
    <div class="content">
        <div class="wrapper">
            <div class="description clearFix newsblock slick-initialized slick-slider">
                <div tabindex="0" aria-live="polite" class="slick-list draggable">
                    <div class="slick-track">
                        <?php foreach ($posts as $post): ?>
                            <p aria-hidden="true" data-slick-index="4" class="slideTizerNews slick-slide slick">
                                <?=$post->title?>
                                <br>
                                <?=$post->text?>
                                <?=Html::img('/'.$post->picture, ['width'=>'100%', 'style'=>'max-height:200px;'])?>
                                <span class="news-date"><?=date('d M Y')?></span>
                            </p>
                            <div style="clear:both;"></div>
                        <?php endforeach; ?>

                    </div>
                </div>
                <!--                <button style="display: block;" type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>
                                <button style="display: block;" type="button" data-role="none" class="slick-next" aria-label="next">Next</button> -->
            </div>
<!--            <div class="description">
                <p class="alert">
                    В случаях необходимости получения скорой и неотложной медицинской помощи (при заболеваниях и состояниях, требующих срочного медицинского вмешательства) следует обращаться в единую диспетчерскую службу скорой медицинской помощи по номеру 03 со стационарного телефона или 103 с мобильного телефона, либо по телефону единой диспетчерской службы экстренных служб - 112.
                    Неотложную медицинскую помощь можно также получить при непосредственном обращении в поликлинику по месту жительства в часы работы поликлиники. Неотложная медицинская помощь оказывается без предварительной записи
                </p>
            </div> -->
        </div>
    </div>
</div>
