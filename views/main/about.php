<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Biz haqimizda';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Bizning kompaniya sizga misli ko'rilmagan xizmatlarni taqdim etadi, jumladan:
    </p>

    <ul style="list-style: decimal">
        <li>Halo</li>
        <li>Alo</li>
        <li>Malo</li>
        <li>Falo</li>
        <li>Dulo</li>
    </ul>
</div>
