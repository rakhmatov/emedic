<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'My Yii Application';
?>
<div class="main-index">
    <div class="content">
        <div class="wrapper">
            <div class="tizers slick-initialized slick-slider">
                <div tabindex="0" aria-live="polite" class="slick-list draggable">
                    <div style="opacity: 1; width: 1025px; transform: translate3d(0px, 0px, 0px);" class="slick-track">
                        <div aria-hidden="false" data-slick-index="0" class="tizer n3 slick-slide slick-active">
                            <div class="data" onclick="location.href='/service/record/profiles'">
                                <span class="kvrachu"><span class="lock" title="Требуется авторизация"></span></span>
                                <h2>Doktor qabuliga yozilish</h2>
                                <p>Yashash joyi boyicha doktor qabuliga yozilish. Avtorizatsiya talab etiladi.</p>
                            </div>
                        </div>
                        <div aria-hidden="false" data-slick-index="2" class="tizer n3 slick-slide slick-active">
                            <div class="data" onclick="location.href='/service/schedule'">
                                <span class="rasp"></span>
                                <h2>Doktorlar ish rejimi</h2>
                                <p>Davlat klinikasi doktorlar ish rejimi.</p>
                            </div>
                        </div>
                        <div aria-hidden="false" data-slick-index="3" class="tizer n3 slick-slide slick-active">
                            <div class="data" onclick="location.href='/service/hospitals'">
                                <span class="medorg"></span>
                                <h2>Медицинские организации</h2>
                                <p>Государственные и муниципальные медицинские организации региона</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="description clearFix newsblock slick-initialized slick-slider">
                <div tabindex="0" aria-live="polite" class="slick-list draggable">
                    <div style="opacity: 1; width: 3584px; transform: translate3d(0px, 0px, 0px);" class="slick-track">
                        <?php foreach ($posts as $post): ?>
                        <p style="width: 462px; height: 200px;" aria-hidden="true" data-slick-index="4" class="slideTizerNews slick-slide slick">
                            <?=$post->title?>
                            <br>
                            <?=$post->text?>
                            <?=Html::img('/'.$post->picture, ['width'=>'100%', 'style'=>'max-height:200px;'])?>
                            <span class="news-date"><?=date('d M Y')?></span>
                        </p>
                        <?php endforeach; ?>

                    </div>
                </div>
<!--                <button style="display: block;" type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>
                <button style="display: block;" type="button" data-role="none" class="slick-next" aria-label="next">Next</button> -->
            </div>
<!--            <div class="description">
                <p class="alert">
                    В случаях необходимости получения скорой и неотложной медицинской помощи (при заболеваниях и состояниях, требующих срочного медицинского вмешательства) следует обращаться в единую диспетчерскую службу скорой медицинской помощи по номеру 03 со стационарного телефона или 103 с мобильного телефона, либо по телефону единой диспетчерской службы экстренных служб - 112.
                    Неотложную медицинскую помощь можно также получить при непосредственном обращении в поликлинику по месту жительства в часы работы поликлиники. Неотложная медицинская помощь оказывается без предварительной записи
                </p>
            </div> -->
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
