<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Ish jadvali';
$this->params['breadcrumbs'][] = $this->title;
$date = strtotime(date('Y-m-d 09:00:00'), time());
$writtenDates = [];
foreach ($writtens as $written){
    $writtenDates[] = $written->date;
}
?>
<div class="wrapper">
    <div class="activeWrDocHead">
        <div class="activeWrDocContainer">
            <img src="/<?=$doctor->picture?>" class="doc-photo">
            <span class="docname"><?=$doctor->full_name?></span>
            <span class="docspec"><?=$doctor->getProfession()->one()->prof_name?></span>
        </div>
        <div class="addictiveRightLinks">
				<span class="selToggler">
					<a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/timetable', 'id'=>$doctor->id])?>" class="selectedToggler">Ish grafiki</a>
					<a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/info', 'id'=>$doctor->id])?>">Doktor ma'lumotlari</a>
                </span>
        </div>
    </div>
    <div class="timeTableWrapper">
        <div class="timeTableContentSlideContainer slick-initialized slick-slider">
            <div tabindex="0" aria-live="polite" class="slick-list draggable">
                <div style="opacity: 1; width: 3072px; transform: translate3d(0px, 0px, 0px);" class="slick-track">
                    <table style="width: 1024px;" aria-hidden="false" data-slick-index="0" class="timeTable slick-slide slick-active" width="1024">
                        <tbody><tr>
                            <?php
                            $d = date('Y-m-d H:i:s');
                            for ($i=0;$i<14;$i++):
                                $d = date('Y-m-d H:i:s', strtotime($d." +1 day"));
                                ?>
                            <th scope="col"><?=date('d-F', strtotime($d))?><br>
                                <?=date('D', strtotime($d))?></th>
                            <?php endfor; ?>
                        </tr>
                        <?php for($i=0;$i<24;$i++):?>
                            <tr>
                            <?php for($j=0;$j<14;$j++):
                                $isItBusy = \app\models\WrittenToDoctor::find()->where(['doctor'=>$doctor->id, 'date'=>$date])->one();
                                ?>
                                <td id="<?=$date?>" class="<?=(date('H', $date) != '13') ? (($isItBusy) ? 'busy' : 'free') : 'annot'?>" title="<?=(date('H', $date) != '13') ? '' : 'Tushlik'?>" rel="13 марта, понедельник 13:00" desc="Кольпоскопия"><?=date('H:i', $date)?></td>
                                <?php $date+=60*60*24; ?>
                            <?php endfor; ?>
                            <?php
                            $date-=60*60*24*14;
                            $date+=60*20;
                            ?>
                            </tr>
                        <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
<!--    <div class="preMapLpuInfoBlock">
        <h2>ГБУЗ АО "ГП №2"</h2>
        <span class="lpuAddr">г Астрахань, ул Соликамская, д 8</span>
        <span class="lpuTel">31-78-60 (регистратура); 31-78-84 (запись к врачу)			</span>
        <div class="separator20"></div>
        <div class="docNote">
        </div>
        <script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
        <div class="mapContainer">
            <div id="map"></div>
        </div>
        <script type="text/javascript">
            ymaps.ready(function() {
                map = new ymaps.Map('map', {
                    zoom: 16,
                    controls: ['smallMapDefaultSet'],
                    center: [54.83, 37.11]
                });
                map.behaviors.disable('scrollZoom');
                map.controls
                    .remove('searchControl')
                    .remove('geolocationControl');

                var marker = new ymaps.Placemark([46.382533, 48.096474], {
                    balloonContentBody: '<div class="gmap-popupContent"><h2>Поликлиническое Отделение №1</h2><p>г Астрахань, ул Соликамская, д 8<br/>31-78-60 (регистратура); 31-78-84 (запись к врачу)</p></div>',
                    hintContent: 'ГБУЗ АО "ГП №2" Поликлиническое отделение №1'
                });

                map.geoObjects.add(marker);
                map.setCenter(marker.geometry.getCoordinates());

                if (map.getZoom() > 16) map.setZoom(16);
            });
        </script>
        <div class="popup regInfo">
            <span class="close"></span>
            <p>Выберите человека из картотеки для записи</p>
            <ul class="personList">
                <li><a href="#" onclick="selectPerson(908205, 2365); return false;"><?=\Yii::$app->user->identity->name?> <?=\Yii::$app->user->identity->family?></a></li>
            </ul>
        </div>
    </div> -->
</div>

<div aria-labelledby="ui-id-2" aria-describedby="ui-id-1" role="dialog" tabindex="-1" style="position: absolute; height: auto; width: 570px; top: 376px; left: 667.5px; display: none; z-index: 101;" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable ui-resizable Dialog">
    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle">
        <span class="ui-dialog-title" id="ui-id-2">&nbsp;</span>
        <button title="Close" role="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" type="button">
            <span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Close</span>
        </button>
    </div>
    <div style="display: block; width: auto; min-height: 100px; max-height: none; height: auto;" id="ui-id-1" class="popup regAgree ui-dialog-content ui-widget-content">
        <span class="close"></span>
        <h1><?=\Yii::$app->user->identity->name?> <?=\Yii::$app->user->identity->family?><br>
            <small>Doktor qabuliga yozilish</small></h1>
        <p><span class="label">Mutahasislik:</span><span class="labelVal"><?=$doctor->getProfession()->one()->prof_name?></span></p>
        <p><span class="label">Mutahasis:</span><span class="labelVal"><?=$doctor->full_name?></span></p>
        <p><span class="label">Maskan:</span><span class="labelVal"><?=$doctor->getWork()->one()->name?></span></p>
        <p><span class="label">Address:</span><span class="labelVal"><?=$doctor->getWork()->one()->getRegion()->one()->name?>, <?=$doctor->getWork()->one()->address?></span></p>
        <p><span class="label">Sana va vaqt:</span><span class="labelVal"><span class="record-datetime">23 марта, четверг 15:00</span></span></p>
        <div style="display: none;" class="warning" id="day_description_warning">
            <p>Примечание врача: <span class="day_description"></span></p>
        </div>
        <hr>

        <p><input id="email_notify" name="email_notify" type="checkbox"><label for="email_notify" class="space">Напомнить по эл. почте</label></p>
        <div class="clearFix"></div>
        <p><label class="space">Время напоминания:</label><select aria-hidden="true" class="select2-hidden-accessible" tabindex="-1" disabled="" id="notify_time" name="notify_time"><option value="1">За час</option><option value="2">За 2 часа</option><option value="4">За 4 часа</option><option value="8">За 8 часов</option><option value="24" selected="">За сутки</option><option value="48">За 2 суток</option><option value="96">За 4 суток</option></select><span style="width: auto;" dir="ltr" class="select2 select2-container select2-container--default select2-container--disabled"><span class="selection"><span aria-labelledby="select2-notify_time-container" tabindex="-1" class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false"><span title="За сутки" id="select2-notify_time-container" class="select2-selection__rendered">За сутки</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></p>
        <hr>
<!--        <div class="warning">По данным системы Вы записываетесь в медицинскую организацию не по месту своего прикрепления.<br>
            При отсутствии направления к выбранному специалисту в бесплатном приёме Вам могут отказать.<br>
        </div>-->
        <p><input id="agree" type="checkbox"><label class="agree space" for="agree">Я согласен с правилами</label></p><div class="separator20"></div>
        <p><a href="<?=\Yii::$app->urlManager->createUrl(['service/record/add', 'id'=>$doctor->id])?>" class="button record-button disabled">Tasdiqlash</a> <a class="button grey record-close-button">Bekor</a></p>
    </div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-n"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-e"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-s"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-w"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-sw"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-ne"></div>
    <div style="z-index: 90;" class="ui-resizable-handle ui-resizable-nw">

    </div>
</div>