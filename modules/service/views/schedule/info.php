<div class="wrapper">
    <div class="activeWrDocHead">
        <div class="activeWrDocContainer">
            <span class="docname"><?=$doctor->full_name?></span>
            <span class="docspec"><?=$doctor->getProfession()->one()->prof_name?></span>
        </div>
        <div class="addictiveRightLinks">
				<span class="selToggler">
					<a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/timetable', 'id'=>$doctor->id])?>">Ish jadvali</a>
					<a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/info', 'id'=>$doctor->id])?>" class="selectedToggler">Doktor ma'lumotlari</a>
                </span>
        </div>
    </div>
    <div class="docCardBlock">
        <div class="docPhoto">
            <img style="width:160px;" src="/<?=$doctor->picture?>">
        </div>
        <div class="docInfoBlock">
            <?=$doctor->about?>
        </div>
        <div class="clearFix"></div>
    </div>
</div>