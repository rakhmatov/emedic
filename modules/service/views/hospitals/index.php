<?php
/* @var $this yii\web\View */
?>

<div class="wrapper">
    <h1>Tibbiy muassasalar</h1>
    <div class="medOrganizationsTable">
        <div class="moTableDetail">
            <?php foreach ($medcenter as $mc): ?>
            <a class="moDetail" href="<?=\Yii::$app->urlManager->createUrl(['service/hospitals/view', 'id'=>$mc->id])?>" style="display: block;">
                <div class="leftAlignmentTableCell"></div>
                <div class="leftMoContent">
                    <span class="lpu-name"><?=$mc->name?></span><br>
                    <span class="lpu-address"><?=$mc->address?></span>
                </div>
                <div class="mark-stars "></div>
            </a>
            <?php endforeach; ?>
        </div>

    </div>
</div>