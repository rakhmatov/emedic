<?php
/* @var $this yii\web\View */
?>
<div class="wrapper">
    <input value="13003269" id="Lpu_id" type="hidden">
    <h1>
        <?=$medcenter->name?>
        <span class="pseudoTab">
            <span class="org_light">
                <a href="<?=\Yii::$app->urlManager->createUrl(['service/hospitals/view', 'id'=>$medcenter->id])?>">Umumiy ma'lumot</a>
            </span>
            <span class="selected spec_dark">
                <a href="<?=\Yii::$app->urlManager->createUrl(['service/hospitals/doctors', 'id'=>$medcenter->id])?>">Mutahasislar</a>
            </span>
        </span>
    </h1>
    <table width="100%">
        <tbody><tr>
            <th scope="col" style="width: 300px;">Mutahasislik</th>
            <th scope="col">Doktor</th>
            <th scope="col">Status</th>
        </tr>
        <?php foreach ($medcenter->getDoctors()->all() as $doctor) {?>
            <tr>
                <td><?=$doctor->getProfession()->one()->prof_name?></td>
                <td><a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/info', 'id'=>$doctor->id])?>"><?=$doctor->full_name?></a></td>
                <td><span class="Schedule not_free"></span></td>
            </tr>
        <?php }?>
        </tbody>
    </table>

    <div class="mapContainer">
        <div id="map"></div>
    </div>
</div>