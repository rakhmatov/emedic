<?php
/* @var $this yii\web\View */
?>
<div class="wrapper">
    <input value="13003269" id="Lpu_id" type="hidden">
    <h1>
        <?=$medcenter->name?>
        <span class="pseudoTab">
            <span class="selected org_light">
                <a href="<?=\Yii::$app->urlManager->createUrl(['service/hospitals/view', 'id'=>$medcenter->id])?>">Umumiy ma'lumot</a>
            </span>
            <span class="spec_dark">
                <a href="<?=\Yii::$app->urlManager->createUrl(['service/hospitals/doctors', 'id'=>$medcenter->id])?>">Mutahasislar</a>
            </span>
        </span>
    </h1>
    <div class="lpuDesc">
        <div class="data">
            <p><span class="label">Nomi</span><span><?=$medcenter->name?></span></p>
            <p><span class="label">Addressi</span><span><?=$medcenter->getRegion()->one()->name?>, <?=$medcenter->address?></span></p>
            <p><span class="label">Sayt</span><span><a href="http://gorpol2@mail.ru">http://gorpol2@mail.ru</a></span></p>
            <p><span class="label">Telefon</span><span>(8512)31-78-85</span></p>
            <p><span class="label">Bosh doktor</span><span>ВАСИЛЬЕВА ОЛЬГА ВЛАДИМИРОВНА</span></p>
            <p><span class="label">Bo'linma</span>
                <span class="lpudepart">
                    <span><strong>Дневной стационар (ул.бабаевского,35/4)</strong><br>Астрахань, Ул. Бабаевского, 35, Корп. 4<br></span><span><strong>Дневной стационар-взрослые (ул.соликамская,8)</strong> <a href="#map" onclick="moveToUnit(31);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br></span><span><strong>Дневной стационар-дети (ул.соликамская,8)</strong> <a href="#map" onclick="moveToUnit(32);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br></span><span><strong>Параклиника (ул.аксакова,6/1)</strong> <a href="#map" onclick="moveToUnit(41);">Показать на карте</a><br>Астрахань, Ул. Аксакова, 6, Корп. 1<br></span><span><strong>Параклиника (ул.бабаевского,35/4)</strong><br>Астрахань, Ул. Бабаевского, 35, Корп. 4<br></span><span><strong>Параклиника-взрослые (ул.соликамская,8)</strong> <a href="#map" onclick="moveToUnit(34);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br></span><span><strong>Параклиника-дети (ул.соликамская,8)</strong> <a href="#map" onclick="moveToUnit(35);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br></span><span><strong>Педиатрическое отделение №1</strong> <a href="#map" onclick="moveToUnit(27);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br>31-78-70 (регистратура); 31-78-84 (запись к врачу)</span><span><strong>Педиатрическое отделение №2</strong> <a href="#map" onclick="moveToUnit(40);">Показать на карте</a><br>Астрахань, Ул. Аксакова, 6, Корп. 1<br>31-78-90 (регистратура);  31-78-74 (запись к врачу)</span><span><strong>Поликлиническое отделение №1</strong> <a href="#map" onclick="moveToUnit(26);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br>31-78-60 (регистратура); 31-78-84 (запись к врачу)</span><span><strong>Поликлиническое отделение №2</strong><br>Астрахань, Ул. Бабаевского, 35, Корп. 4<br>31-78-80 (регистратура); 31-78-74 (запись к врачу)</span><span><strong>Стационар на дому (ул.бабаевского,35/4)</strong><br>Астрахань, Ул. Бабаевского, 35, Корп. 4<br></span><span><strong>Стационар на дому-взрослые (ул.соликамская,8)</strong> <a href="#map" onclick="moveToUnit(33);">Показать на карте</a><br>Астрахань, Ул. Соликамская, 8<br></span>
                </span>
            </p>
        </div>
        <div class="photo">
            <a href="#"></a>
        </div>
    </div>

    <div class="mapContainer">
        <div id="map"></div>
    </div>
</div>