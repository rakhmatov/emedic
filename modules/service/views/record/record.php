<?php
/* @var $this yii\web\View */
?>
<div class="wrapper">
    <div class="docRecordingBlock">
        <h1><a href="/user/cards"><?=\Yii::$app->user->identity->name?></a> <span>записывается к врачу</span></h1>
        <div class="clientLpuAttachmentsTab"><span>Прикрепление</span></div>
        <div class="clientLpuAttachments">
            <p><span>
				Основное прикрепление:
				</span>
                <a href="/service/hospitals/view/13003269">ГБУЗ АО "ГП №2", г Астрахань, ул Соликамская, д 8</a>
                / <a href="/service/hospitals/regions/13003269#174">участок 1</a>											 / <a href="/service/record/908205/36852/timetable">Бодрова Разия Фазлыевна</a>
            </p>
            <p><a class="isAttachmentWrong" href="#">Сообщить о неверном прикреплении</a></p>
        </div>
        <div class="clearFix"></div>
    </div>        <ul class="steps step1">
        <li><a href="/service/record/908205/profiles">Выберите специальность</a></li>
        <li><span>Специалист</span></li>
        <li><span>Дата и время</span></li>
    </ul>


    <h1>Поиск врача</h1>
    <form class="doc_search">
        <p class="inputFieldwithCase">
            <input value="908205" id="Person_id" type="hidden">
            <input autocomplete="off" class="autoCompleteInput ui-autocomplete-input" placeholder="Введите ФИО или специальность врача" type="text">
            <a class="button search"></a>
        </p>
    </form>

    <dl class="selectDoc filtered">
        <dt class="allDoctors selected" style="display: none;"></dt>
        <dd>
            <ul>
                <li>
                    <ul>
                        <span class="big">А</span>
                        <?php foreach ($professions as $profession){ ?>
                            <li><?=\yii\bootstrap\Html::a($profession->prof_name, ['service/record', 'id'=>$profession->id, 'title'=>"doctors", 'step'=>2])?></li>
                        <? } ?>
                        <li><a href="/service/record/908205/402182/0/doctors">Акушер - гинеколог</a></li>
                        <li><a href="/service/record/908205/35124/0/doctors">Аллерголог-иммунолог</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">В</span>
                        <li><a href="/service/record/908205/0/203/doctors">Врач общей практики-семейный врач</a></li>
                        <li><a href="/service/record/908205/35231/0/doctors">Врач функциональной диагностики</a></li>
                    </ul></li>
                <li>
                    <ul>
                        <span class="big">Г</span>
                        <li><a href="/service/record/908205/35131/0/doctors">Гастроэнтеролог</a></li>
                        <li><a href="/service/record/908205/35132/0/doctors">Гематолог</a></li>
                    </ul
                </li>
                <li>
                    <ul>
                        <span class="big">Д</span>
                        <li>
                            <a href="/service/record/908205/35136/0/doctors">Дерматовенеролог</a>
                        </li>
                        <li>
                            <a href="/service/record/908205/35206/0/doctors">Детский стоматолог</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">И</span>
                        <li>
                            <a href="/service/record/908205/35148/0/doctors">Инфекционист</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">К</span>
                        <li>
                            <a href="/service/record/908205/35149/0/doctors">Кардиолог</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Л</span>
                        <li><a href="/service/record/908205/35154/0/doctors">Лабораторной диагностики</a></li>
                        <li><a href="/service/record/908205/35161/0/doctors">Лечебная физкультура</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Н</span>
                        <li><a href="/service/record/908205/35173/0/doctors">Невролог</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">О</span>
                        <li><a href="/service/record/908205/35180/0/doctors">Онколог</a></li>
                        <li><a href="/service/record/908205/35184/0/doctors">Оториноларинголог</a></li>
                        <li><a href="/service/record/908205/35185/0/doctors">Офтальмолог</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">П</span>
                        <li><a href="/service/record/908205/35188/0/doctors">Педиатр</a></li>
                        <li><a href="/service/record/908205/35195/0/doctors">Пульмонолог</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Р</span>
                        <li><a href="/service/record/908205/35197/0/doctors">Ревматолог</a></li>
                        <li><a href="/service/record/908205/35198/0/doctors">Рентгенолог</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">С</span>
                        <li><a href="/service/record/908205/35205/0/doctors">Стоматолог</a></li>
                        <li><a href="/service/record/908205/35210/0/doctors">Стоматолог хирург</a></li>
                        <li><a href="/service/record/908205/35208/0/doctors">Стоматолог-ортопед</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Т</span>
                        <li><a href="/service/record/908205/35217/0/doctors">Терапевт и врач общ. практики</a></li>
                        <li><a href="/service/record/908205/35220/0/doctors">Травматолог-ортопед</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">У</span>
                        <li><a href="/service/record/908205/35226/0/doctors">Ультразвуковой диагностики</a></li>
                        <li><a href="/service/record/908205/35228/0/doctors">Уролог</a></li>
                        <li><a href="/service/record/908205/35139/0/doctors">Уролог детский</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Ф</span>
                        <li><a href="/service/record/908205/35229/0/doctors">Физиотерапевт</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Х</span>
                        <li><a href="/service/record/908205/35232/0/doctors">Хирург</a></li>
                        <li><a href="/service/record/908205/35140/0/doctors">Хирург детский</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Ц</span>
                        <li><a href="/service/record/908205/35255/0/doctors">Центр здоровья</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <span class="big">Э</span>
                        <li><a href="/service/record/908205/35242/0/doctors">Эндокринолог</a></li>
                        <li><a href="/service/record/908205/35141/0/doctors">Эндокринолог детский</a></li>
                        <li><a href="/service/record/908205/35243/0/doctors">Эндоскопист</a></li>
                    </ul>
                </li>
            </ul>
        </dd>
    </dl>
</div>