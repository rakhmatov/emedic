<?php
/* @var $this yii\web\View */
?>
<div class="wrapper">
    <div class="docRecordingBlock">
        <h1><a href="/user/cards"><?=\Yii::$app->user->identity->name?> <?=\Yii::$app->user->identity->family?></a> <span>записывается к врачу</span></h1>
        <div class="clientLpuAttachmentsTab"><span>Прикрепление</span></div>
        <div class="clientLpuAttachments">
            <p><span>
				Основное прикрепление:
				</span>
                <a href="/service/hospitals/view/13003269">ГБУЗ АО "ГП №2", г Астрахань, ул Соликамская, д 8</a>
                / <a href="/service/hospitals/regions/13003269#174">участок 1</a> / <a href="/service/record/908205/36852/timetable">Бодрова Разия Фазлыевна</a>
            </p>
            <p><a class="isAttachmentWrong" href="#">Сообщить о неверном прикреплении</a></p>
        </div>
        <div class="clearFix"></div>
    </div>
    <ul class="steps step2">
        <li><a href="/service/record/profiles"><?=$professionName?></a></li>
        <li><a href="/service/record/908205/35205/1/doctors">Выберите врача</a></li>
        <li><span>Дата и время</span></li>
    </ul>
    <h1>Поликлиника прикрепления (ГБУЗ АО "ГП №2")</h1>
    <div class="sortByBlock">
        Упорядочить:
        <span class="selToggler">
            <a class="selectedToggler" href="#" id="sortMyDoctorsByName">по фамилии</a>
            <a href="#" id="sortMyDoctorsByRecord">по ближайшей записи</a>
        </span>
    </div>
    <div class="docsInLpuTable">
        <div class="headerDocsInLpuTable">
            <span class="headerDocsInLpuTableSpec">Специалист</span>
            <span class="headerDocsInLpuTableNearestRec">Ближайшая запись</span>
            <span class="headerDocsInLpuTableSpecMark">Оценка</span>
            <span class="headerDocsInLpuTableSpecFeeds">Отзывы</span>
        </div>
        <div class="docsInLpuTableDetail">
            <?php
            $lastWork = "";
            foreach($doctors as $doctor){ ?>
                <div class="Lpuunit" id="lpuunit_26">
            <?php
                if($lastWork != $doctor->work){ ?>
                    <div class="docsInLpuTableDescr">
                        <a class="lpu-fullName" href="<?=\Yii::$app->urlManager->createUrl(['service/hospitals/view', 'id'=>$doctor->getWork()->one()->id])?>"> <?=$doctor->getWork()->one()->name?></a>
                        <span class="lpu-address"><?=$doctor->getWork()->one()->getRegion()->one()->name?>, <?=$doctor->getWork()->one()->address?></span>
                    </div>
                <? }
                $lastWork = $doctor->work;
                ?>
                    <a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/timetable', 'id'=>$doctor->id, 'step'=>3])?>">
                                    <span class="doc-photo">
                                        <img src="/img/logo.png">
                                    </span>
                        <span class="doc-name"><?=$doctor->full_name?></span>
                        <span class="Freetime hidden">empty</span>
                        <span class="nearest-record"></span>
                        <div class="mark-stars stars0"></div>
                        <span class="count-feedbacks">0</span>
                    </a>
                </div>

            <? } ?>
        </div>
    </div>
    <div class="docsInLpuTablePostInfoBlock">
        <a href="#" class="button grey" id="otherDoctors">Найти специалиста в другой поликлинике</a>
    </div>
</div>