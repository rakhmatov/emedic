<?php
/* @var $this yii\web\View */
?>
<div class="wrapper">
    <div class="docsInLpuTable">
        <div class="headerDocsInLpuTable">
            <span class="headerDocsInLpuTableSpec">Специалист</span>
            <span class="headerDocsInLpuTableNearestRec">Ближайшая запись</span>
            <span class="headerDocsInLpuTableSpecMark">Оценка</span>
            <span class="headerDocsInLpuTableSpecFeeds">Отзывы</span>
        </div>
        <div class="docsInLpuTableDetail">
            <?php
            $lastWork = "";
            foreach($result as $doctor){ ?>
                <div class="Lpuunit" id="lpuunit_26">
                    <a href="<?=\Yii::$app->urlManager->createUrl(['service/schedule/timetable', 'id'=>$doctor->id, 'step'=>3])?>">
                                    <span class="doc-photo">
                                        <img src="/img/logo.png">
                                    </span>
                        <span class="doc-name"><?=$doctor->full_name?></span>
                        <span class="Freetime hidden">empty</span>
                        <span class="nearest-record"></span>
                        <div class="mark-stars stars0"></div>
                        <span class="count-feedbacks">0</span>
                    </a>
                </div>

            <?php } ?>
        </div>
    </div>
</div>