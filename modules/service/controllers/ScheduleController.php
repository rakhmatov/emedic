<?php

namespace app\modules\service\controllers;

use app\models\Doctor;
use app\models\WrittenToDoctor;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Default controller for the `service` module
 */
class ScheduleController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['timetable'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public $defaultAction = 'info';
    public function actionInfo($id)
    {
        $doctor = Doctor::findOne($id);
        return $this->render('info', [
            'doctor' => $doctor,
        ]);
    }
    public function actionTimetable($id)
    {
        $doctor = Doctor::findOne($id);
        $writtens = WrittenToDoctor::find()->where(['doctor'=>$doctor->id])->all();
        return $this->render('timetable', [
            'doctor' => $doctor,
            'writtens' => $writtens,
        ]);
    }
}
