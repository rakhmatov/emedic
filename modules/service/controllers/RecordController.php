<?php

namespace app\modules\service\controllers;

use app\models\Doctor;
use app\models\Medcenter;
use app\models\Profession;
use app\models\WrittenToDoctor;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Record controller for the `service` module
 */
class RecordController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['timetable'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionProfiles($id=null)
    {
        if($id){
            $doctors = Profession::findOne($id)->getDoctors()->orderBy('work')->all();
            return $this->render('record_table', [
                'doctors' => $doctors,
            ]);
        }
        $professions = Profession::find()->orderBy('prof_name')->all();
        return $this->render('profiles', [
            'professions' => $professions,
        ]);
    }

    public function actionDoctors($id=null)
    {
        if($id){
            $doctors = Profession::findOne($id)->getDoctors()->orderBy('work')->all();
            $professionName = Profession::findOne($id)->prof_name;
            return $this->render('record_table', [
                'doctors' => $doctors,
                'professionName' => $professionName,
            ]);
        }
        return $this->redirect([404]);
    }

    public function actionHospitals($id)
    {
        $medcenter = Medcenter::findOne([$id]);
        return $this->render('hospitals');
    }

    public function actionRegions()
    {
        return $this->render('regions');
    }

    /**
     * @param int $id
     * @param int $time
     * @return void|\yii\web\Response
     */
    public function actionAdd($id=1, $time=null)
    {
        $doctor = Doctor::findOne($id);
        $wtd = new WrittenToDoctor();
        $wtd->doctor = $doctor->id;
        $wtd->user = \Yii::$app->user->identity->getId();
        $wtd->date = $time."";
        if($wtd->save()){
            return $this->goBack();
        }
        else{
            var_dump($wtd);return;
            return $this->goHome();
        }
    }

    public function actionSearch(){
        $search = \Yii::$app->request->post('search');
        $doctor = Doctor::find()->where(['like', 'full_name', $search])->all();
        return $this->render('search', [
            'result' => $doctor,
        ]);

    }
}
