<?php

namespace app\modules\service\controllers;

use app\models\Medcenter;
use app\models\Profession;
use yii\web\Controller;

/**
 * Hospital controller for the `service` module
 */
class HospitalsController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex()
    {
        $medcenter = Medcenter::find()->all();
        return $this->render('index', [
            'medcenter' => $medcenter,
        ]);
    }

    public function actionView($id=null)
    {
        $medcenter = Medcenter::findOne([$id]);
        return $this->render('view', [
            'medcenter' => $medcenter,
        ]);
    }

    public function actionDoctors($id=null)
    {
        $medcenter = Medcenter::findOne([$id]);
        return $this->render('doctors', [
            'medcenter' => $medcenter,
        ]);
    }

    public function actionRegions()
    {
        return $this->render('regions');
    }
}
