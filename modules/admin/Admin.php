<?php

namespace app\modules\admin;


use app\models\AccessRule;
use app\models\User;
use yii\filters\AccessControl;

class Admin extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init(){
        parent::init();
    }

    public function behaviors(){
        return [
            'access'=> [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
        ];
    }
}
