<?php

namespace app\modules\admin\controllers\api;

/**
* This is the class for REST controller "ProfessionController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class ProfessionController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Profession';
}
