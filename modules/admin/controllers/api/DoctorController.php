<?php

namespace app\modules\admin\controllers\api;

/**
* This is the class for REST controller "DoctorController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DoctorController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Doctor';
}
