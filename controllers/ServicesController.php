<?php

namespace app\controllers;

use app\models\Medcenter;
use app\models\Profession;

class ServicesController extends \yii\web\Controller
{
    public function actionRecord($id=null)
    {
        if($id){
            $doctors = Profession::findOne($id)->getDoctors()->orderBy('work')->all();
            return $this->render('record_table', [
                'doctors' => $doctors,
            ]);
        }
        $professions = Profession::find()->all();
        return $this->render('record', [
            'professions' => $professions,
        ]);
    }

    public function actionHospitals($id)
    {
        $medcenter = Medcenter::findOne([$id]);
        return $this->render('hospitals');
    }

    public function actionRegions()
    {
        return $this->render('regions');
    }

}
