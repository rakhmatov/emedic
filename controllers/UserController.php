<?php

namespace app\controllers;

use app\models\Region;
use app\models\User;

class UserController extends \yii\web\Controller
{
    public function actionCards()
    {
        return $this->render('cards');
    }

    public function actionLogin()
    {
        return $this->render('login');
    }

    public function actionProfile()
    {
        $regions = Region::find()->all();
        return $this->render('profile', [
            'regions' => $regions,
        ]);
    }
    public function actionRegister()
    {
        $model = new User();
        if(\Yii::$app->request->post()){
            if($model->load(\Yii::$app->request->post())){
                $model->passwd = md5($model->passwd);
                if($model->save()){
                    return $this->redirect(['main/index']);
                }
            }
        }
        $regions = Region::find()->all();
        return $this->render('register', [
            'regions' => $regions,
            'register' => $model,
        ]);
    }

}
