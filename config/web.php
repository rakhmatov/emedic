<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'emedic',
    'name'=>'eMedic Admin Panel',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'main/index',
    'modules'=>[
        'service' => [
            'class' => 'app\modules\service\Module',
//            'layout' => 'main.php',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Admin',
            'layout' => 'main.php',
        ],
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'C00K!EMEDIC',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
//                '<controller:(service)>/<action:(record)>/<id:\d+>/<title:\w+>'=>'<controller>/<action>',
//                '<module>/<controller>/<action>/<id:\d+>/<title:\w+>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:record|schedule>/<id:\d+>/<step:\w+>/<action:doctors|timetable>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:record>/<id:\d+>/<action:doctors>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:record>/<action:profiles>/<id:\d+>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:hospitals>/<action:view|doctors>/<id:\d+>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:record>/<action:regions>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:schedule>/<id:\d+>/<action:(info|timetable)>'=>'<module>/<controller>/<action>',
                '<module:service>/<controller:schedule>/'=>'<module>/record/profiles',
                'site/login'=>'main/login',
                '<module:admin>/<controller>/<action>'=>'<module>/<controller>/<action>',
//                '<controller:(service)>/<action:(record)>/<step:\d+>/<id:\d+>/<title:\w+>'=>'<controller>/<action>',
//                '<controller:(service)>/<action>/<act:\w+>/<id:\d+>'=>'<controller>/<action>',
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
