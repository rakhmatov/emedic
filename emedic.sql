-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: emedic
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `full_name` text,
  `work_from` varchar(255) DEFAULT NULL,
  `about` text,
  `profession` int(10) DEFAULT NULL,
  `work` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profession` (`profession`),
  KEY `work` (`work`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`profession`) REFERENCES `profession` (`id`),
  CONSTRAINT `doctor_ibfk_2` FOREIGN KEY (`work`) REFERENCES `medcenter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'Botir Yaminov','1999','Odom',2,1),(2,'Alimardon To\'rayev','2001','a b c d e f g about doctor',5,1),(3,'Bahodir Umirov','1998','TashMI ni bitkizgan blablabla',1,1),(4,'Jasur Haydarov','1986','Professor blablabla',3,1),(5,'Kamoladdin Seginov','1993','about text .......',1,2),(6,'Shodmonqulov Alisher Umarovich','','TOSHMI tibbiyot institutini 1994-yilda bitkazgan.',9,1);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medcenter`
--

DROP TABLE IF EXISTS `medcenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medcenter` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text,
  `region` int(10) DEFAULT NULL,
  `address` text,
  PRIMARY KEY (`id`),
  KEY `region` (`region`),
  CONSTRAINT `medcenter_ibfk_1` FOREIGN KEY (`region`) REFERENCES `region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medcenter`
--

LOCK TABLES `medcenter` WRITE;
/*!40000 ALTER TABLE `medcenter` DISABLE KEYS */;
INSERT INTO `medcenter` VALUES (1,'Xonqani poliklinikasi',2,'Bilmiyman ko\'chasi 2 uy'),(2,'Urganch poliklinikasi',7,'Aeroport ko\'chasi 4 uy');
/*!40000 ALTER TABLE `medcenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `data_published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `user` (`user`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,2,'2017-05-27 19:49:11',1,'Test','Halo halo halooooooooooooooo'),(2,2,'2017-05-27 19:49:54',1,'Yangilik','blablabla blasdlgasdfk asd;flkjaslekj asldfkjasel;jgsdfogj asdljfalskdjflkjals alsdkjfasldkfj laksjdflkajsdflkjasrkdjgsndoi;ljf asdlfkjaseldgkjsldkfasf asdf'),(3,2,'2017-05-27 21:34:15',1,'Hush kelibsiz!','Ushbu emedic.uz saytida siz elektron ravishda shifokorlar qabuliga yozilishingiz mumkin.');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profession`
--

DROP TABLE IF EXISTS `profession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profession` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `prof_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profession`
--

LOCK TABLES `profession` WRITE;
/*!40000 ALTER TABLE `profession` DISABLE KEYS */;
INSERT INTO `profession` VALUES (1,'Travmatolog'),(2,'Stamatolog'),(3,'Kardiolog'),(4,'Urolog'),(5,'Androlog'),(6,'Lor'),(7,'Xirurg'),(8,'Neyroxirurg'),(9,'Nevrolog'),(10,'Akusher - Ginekolog'),(11,'Allergolog-Immunolog'),(12,'Gostroentrolog'),(13,'Dermotovenerolog'),(14,'Bolalar stomatologi'),(15,'Infeksionist'),(16,'Pediatr'),(17,'Revmotolog'),(18,'Rentgenolog'),(19,'Endokrinolog'),(20,'Endoskopist');
/*!40000 ALTER TABLE `profession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (2,'Xonqa'),(3,'Bog\'ot'),(4,'Xazarasp'),(5,'Shovot'),(6,'Qo\'shko\'pir'),(7,'Urganch'),(8,'Xiva'),(9,'Yangiariq');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (2,'ADMIN'),(3,'USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `validated` tinyint(2) DEFAULT NULL,
  `passwd` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `new_passwd` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `role` int(10) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `family` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `father_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `birth_day` int(10) DEFAULT NULL,
  `birth_month` int(10) DEFAULT NULL,
  `birth_year` int(10) DEFAULT NULL,
  `region` int(10) DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inn` int(24) DEFAULT NULL,
  `email_notify` tinyint(2) DEFAULT NULL,
  `sms_notify` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `region` (`region`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role`) REFERENCES `role` (`id`),
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`region`) REFERENCES `region` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'admin@emedic.uz',1,'0587830151a86bb2fea8214b426c6371',NULL,2,NULL,'Ortiqov','Ortiq','Ortiqovich',12,11,1992,2,'Narimon ko\'chasi 1 uy','998977777777',123456789,1,1),(3,'emedic@emedic.uz',1,'1be1bdb5a5b960c1dd11d174767272e4',NULL,3,NULL,NULL,'Olimjon','Olimovich',1,1,1992,4,'','',NULL,NULL,NULL),(4,'jummi@emedic.uz',1,'6a3047dac2df4544e5c05a7d5d153b17',NULL,3,NULL,'Durdiev','Otirdi','Yotvoldiev',2,3,1995,7,'BIlmiydi o\'ziyam','998977777773',2147483647,NULL,NULL),(5,'jumavoy@jummi.uz',NULL,'fd764d8a2ca5ed96ffa133994f740083',NULL,NULL,NULL,'Jumavoy','Jumavoy','Jumavoy',5,5,1995,5,'BIladi lekin etmidi','9989774377776',1241423231,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work`
--

DROP TABLE IF EXISTS `work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `medcenter` int(10) DEFAULT NULL,
  `doctor` int(10) DEFAULT NULL,
  `workfrom` time(6) DEFAULT NULL,
  `workto` time(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `medcenter` (`medcenter`),
  KEY `doctor` (`doctor`),
  CONSTRAINT `work_ibfk_1` FOREIGN KEY (`doctor`) REFERENCES `doctor` (`id`),
  CONSTRAINT `work_ibfk_2` FOREIGN KEY (`medcenter`) REFERENCES `medcenter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work`
--

LOCK TABLES `work` WRITE;
/*!40000 ALTER TABLE `work` DISABLE KEYS */;
INSERT INTO `work` VALUES (1,1,1,'06:18:00.000000','14:00:00.000000');
/*!40000 ALTER TABLE `work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `writtenTodoctor`
--

DROP TABLE IF EXISTS `writtenTodoctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `writtenTodoctor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `doctor` int(10) DEFAULT NULL,
  `user` int(10) DEFAULT NULL,
  `date` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `writtenTodoctor_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `writtenTodoctor`
--

LOCK TABLES `writtenTodoctor` WRITE;
/*!40000 ALTER TABLE `writtenTodoctor` DISABLE KEYS */;
INSERT INTO `writtenTodoctor` VALUES (1,4,2,'1495910757'),(2,4,2,'1495912291'),(3,3,2,'1495912498'),(4,1,2,'1495913066'),(5,5,2,'1495913088');
/*!40000 ALTER TABLE `writtenTodoctor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-28 10:45:08
