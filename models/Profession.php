<?php

namespace app\models;

use Yii;
use \app\models\base\Profession as BaseProfession;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "profession".
 */
class Profession extends BaseProfession
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
