<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article as ArticleModel;
use yii\helpers\BaseFileHelper;

/**
* Article represents the model behind the search form about `app\models\Article`.
*/
class PictureUpload extends Model
{
        /**
        * @inheritdoc
        */

        public $imageFile;

        public function rules()
        {
                return [
                [['imageFile'], 'file', 'skipOnEmpty'=>true, 'extensions'=>['png','jpg','jpeg', 'gif']],
                ];
        }

        /**
        * @inheritdoc
        */

        public function upload($pathToSave, $fileName){
                if($this->validate()){
                        if(!is_dir($pathToSave)) BaseFileHelper::createDirectory($pathToSave);
                        if (!$this->imageFile->saveAs($pathToSave.$fileName)) {
                    	    echo $this->imageFile->error;
                    	    return false;
                        }
                        //var_dump($pathToSave.$fileName);
                        return true;
                } else {
                        //var_dump('not validated Izzat');
                        return false;
                }
        }
}