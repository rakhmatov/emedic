<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "profession".
 *
 * @property integer $id
 * @property string $prof_name
 *
 * @property \app\models\Doctor[] $doctors
 * @property string $aliasModel
 */
abstract class Profession extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profession';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prof_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'prof_name' => Yii::t('app', 'Prof Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctors()
    {
        return $this->hasMany(\app\models\Doctor::className(), ['profession' => 'id']);
    }


    
    /**
     * @inheritdoc
     * @return \app\models\query\ProfessionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProfessionQuery(get_called_class());
    }


}
