<?php

namespace app\models;

use Yii;
use \app\models\base\WrittenToDoctor as BaseWrittenToDoctor;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "writtenTodoctor".
 */
class WrittenToDoctor extends BaseWrittenToDoctor
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
