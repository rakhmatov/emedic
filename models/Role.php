<?php

namespace app\models;

use Yii;
use \app\models\base\Role as BaseRole;

/**
 * This is the model class for table "role".
 */
class Role extends BaseRole
{
    const ADMIN = 2;

    public function getId(){
        return $this->id;
    }

}
