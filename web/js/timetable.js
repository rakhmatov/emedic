$(document).ready(function() {
	
	$.ui.dialog.prototype._focusTabbable = function(){};
	$( "td.free" ).click(function() {

		if ( $(".popup.regAgree").length ) {
			$(".popup.regAgree").dialog({
				dialogClass: "Dialog",/*Dialog добавляем всем попапам. Второй класс для специфики.*/
				minWidth: 570,
				modal: true
			})

			$('.record-datetime').html($(this).attr('rel'));
			selected_record = $(this).attr('id');

			var desc = $(this).attr('desc');
			if ( desc ) {
				$('#day_description_warning').show();
				$('.day_description').html(desc);
			} else {
				$('#day_description_warning').hide();
				$('.day_description').html('');
				$('.day_description').html('');
			}
			
			var title = $(this).attr('title');
			if ( title == 'Платный прием' ) {
				$('#pay_warning').show();
			} else {
				$('#pay_warning').hide();
			}
			
			$( "#email_time" ).prop('disabled', true);
			$( "#sms_time" ).prop('disabled', true);
			
			$( "#email_notify" ).prop('checked', false);
			$( "#sms_notify" ).prop('checked', false);
		} else {
			if ( $(".popup.regInfo").length ) {
				selected_record = $(this).attr('id');
				if ( $('.personList li').length == 1 ) {
					$('.personList li:first a').click();
				} else {
					$(".popup.regInfo").dialog({
						dialogClass: "Dialog",/*Dialog добавляем всем попапам. Второй класс для специфики.*/
						minWidth: 370,
						modal: true
					})
				}
			}
		}
	})
	
	$(".record-close-button").click(function() {
		$(this).parent().parent().dialog( "close" );
		return false;
	});
	$(".record-button").click(function() {
		if ( $(this).hasClass('disabled') ) {
			return false;
		}
		var query = '?';
		if ( $('#email_notify').prop('checked') ) {
			query += 'email_notify=1&'
		}
		if ( $('#sms_notify').prop('checked') ) {
			query += 'sms_notify=1&'
		}
		if ( $('#email_notify').prop('checked') || $('#sms_notify').prop('checked') ) {
			query += 'notify_time=' + $('#notify_time').val()
		}
		// Нажал один раз и хватит, подожди чуток
		$( "#agree").attr("checked", false);
		$(this).addClass('disabled');

		// window.location.href = this.href + '/' + selected_record +'/record' + query;
		window.location.href = this.href + '&time=' + selected_record;
		return false;
	});
	
	$( "#agree" ).change(function() {
		$( ".record-button" ).toggleClass('disabled', !$(this).is(':checked'));
		return false;
	})

	
	$( "#email_notify" ).click(function() {
		$( "#notify_time" ).prop('disabled', !(this.checked || $( "#sms_notify" ).prop('checked')) );
	})
	
	$( "#sms_notify" ).click(function() {
		$( "#notify_time" ).prop('disabled', !(this.checked || $( "#email_notify" ).prop('checked')) );
	})
	
	if ( jQuery.fn.select2 ) {
		$("select").select2({
			width: "element",
			placeholder: COMBOBOX_LOADING,
			formatNoMatches: function (term) {
				return COMBOBOX_NOT_FOUND
			},
			minimumResultsForSearch: 30
		});
	}
	
	$('.btnCancelRecord').click(function(){
        id = this.id;
        if (confirm(DASHBOARD_CANCEL_RECORD_QUESTION)) {
            $.ajaxSetup({
                cache: false
            });
            $.ajax({
                url: '/service/record/cancelRecord/' + id,
                dataType:'json',
                success: function(data) {
                    if(data.success) {
                        window.location.reload();
                    }
                },
                error: function(){
                    $.alert(RECORD_CANCEL_ERROR)
                }
            });
        }
    });
	
	function startRecord() {
		var hash = window.location.hash.substring(1);
		if ( hash ) {
			document.getElementById(hash).scrollIntoView();
			$( "td.free#" + hash ).click();
		}
	}
	
	startRecord();
	
	if ( $('.timeTableWrapper') ) {
		$('.timeTableWrapper .timeTableContentSlideContainer').slick({
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 100,
			nextArrow: '.timeTableWeekArrowsRight',
			prevArrow: '.timeTableWeekArrowsLeft',
		});
		
		$('.timeTableWrapper .timeTableContentSlideContainer').on('afterChange', function(event, slick, currentSlide, nextSlide){
		  setTimeTableLabel($('.timeTableWeekText'+(slick.currentSlide+1)).html())
		});
		
		var setTimeTableLabel = function(t){
			$(".timeTableWeekText")[0].innerHTML = t
		};
		
	}

	//три точки
	$("body table.timeTable .yellowTableBlock span").each(function(a,b){
		if(b.scrollHeight>36)
		//if(b.clientHeight<b.scrollHeight)
		{
			$(this).addClass('treeDotted');
			var hiddenTooltip = document.createElement( "div" );
			hiddenTooltip.className = 'tableheader-yellow-hiddentooltip';
			hiddenTooltip.innerHTML = b.innerHTML;
			$(this).parents('td').append(hiddenTooltip);
		}
	})
	
	$( "table.timeTable td.annot" ).hover(
		function() {
			var a_id = $(this).attr('class').match(/ttg-annot-([0-9a-z]+)/i)[1];
			$('.ttg-annot-'+a_id).addClass('annot-active');
		},
		function() {
			$('table.timeTable td.annot').removeClass('annot-active');
		}
	);

	$( ".isAttachmentWrong" ).click(function() {
		$(".popup.warningAttach").dialog({
			dialogClass: "Dialog warningAlertCancelInvite",
			minWidth: 445,
			modal: true
		})
		return false;
	});
	
});

function selectPerson(person_id, msf_id) {
	location.href = '/service/record/' + person_id + '/' + msf_id + '/timetable#'+selected_record;
	return false;
}