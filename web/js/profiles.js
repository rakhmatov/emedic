$(document).ready(function() {
	$( "#filterSpecDoc" ).prop("checked", true);	
	$( ".mainDoctors" ).click(function() {
		$( this ).siblings( ".allDoctors" ).removeClass("selected");
		$( this ).addClass("selected");
		return false;
	})
	
	$( ".allDoctors" ).click(function() {
		$( this ).siblings( ".mainDoctors" ).removeClass("selected");
		$( this ).addClass("selected");
		return false;
	})
	
	$( ".autoCompleteInput" ).autocomplete({
		source: '/service/schedule/search',
		minLength: 3,
		delay: 500,
		select: function( event, ui ) {
			location.href = '/service/record/' + $('#Person_id').val() + '/' + ui.item.id + '/timetable';
			$(this).val('');
			event.preventDefault();
			return false;
		},
	})
	.autocomplete( "widget" ).addClass( "autoCompleteInput_menu" );
	$( ".autoCompleteInput" ).autocomplete( "instance" )._renderItem = function( ul, item ) {
		var substr_pos = item.label.toLowerCase().indexOf(this.term.toLowerCase());
		return $( "<li></li>" ) 
		.data( "item.autocomplete", item )
		.append( item.label.substring(0, substr_pos) )
		.append( '<span class="coincident_text">' + item.label.substring(substr_pos, this.term.length + substr_pos ) + '</span>' )
		.append( item.label.substring(substr_pos + this.term.length ) )
		.appendTo( ul );
    };

	$( ".isAttachmentWrong" ).click(function() {
		$(".popup.warningAttach").dialog({
			dialogClass: "Dialog warningAlertCancelInvite",
			minWidth: 445,
			modal: true
		})
		return false;
	});
	
	$( "#filterSpecDoc" ).change (function(e, x){
		var prt = $(this).parents("dl");
		prt.toggleClass('filtered', $(this).prop( "checked" ));
	});
	
});