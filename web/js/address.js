/**
 * Функционал по работе с комбобоксами адресов
 */
$(document).ready(function() {

    $('#territory-id').change(function() {
        var territory_id = $(this).val();
        if (territory_id == 0) {
            resetSelect($('#town-id, #street-id'));
            disableSelect($('#town-id, #street-id'));
        } else {
            resetSelect($('#town-id, #street-id'));
            disableSelect($('#town-id, #street-id'));

            $.ajax({
                url: '/address/getTerritoryLocations/' + territory_id,
                dataType:'json',
                success: function(data) {
                    setSelectOptions($('#town-id'), data.towns);

                    if(data.is_city) {
                        setSelectValue($('#town-id'), data.city_id);
                    }
                    
                    if(data.is_town) {
                        setSelectValue($('#town-id'), data.town_id);
                    }
                    
                    if( data.towns.length > 0 ) {
                        enableSelect($('#town-id'));
                    }
                    
                    /*if(data.streets.length > 0) {
                        enableSelect($('#street-id'));
                    }*/
                }
            });
        }
    });
    
    $('#town-id').change(function() {
        var town_id = $(this).val();

        disableSelect($('#street-id'));

        if ( town_id == null || town_id == 0 ) {
            resetSelect($('#street-id'));
            disableSelect($('#street-id'));
        } else {
            $.ajax({
                url: '/address/getTownLocations/' + town_id,
                dataType:'json',
                success: function(data) {
                    resetSelect($('#street-id'));
                    if(data.streets.length > 0) {
                        enableSelect($('#street-id'));
                        setSelectOptions($('#street-id'), data.streets);
                    }
                }
            });
        }
    });

    var disableSelect = function(elements) {
        $.each(elements, function() {
            $(this).prop( "disabled", true );
        });
    };

    var enableSelect = function(elements) {
        $.each(elements, function() {
            $(this).prop( "disabled", false );
        });
    };

    var resetSelect = function(elements) {
        $.each(elements, function() {
            if (this.type == 'select-one') {
                $(this)
                    .empty()
                    .append('<option value="0">' + COMBOBOX_NOT_SELECTED + '</option>')
                    .val(0);
                $(this).val("0").trigger("change");
                disableSelect(this);
            } else {
                $(this).val("0").trigger("change");
            }
        });
    };

    var setSelectOptions = function( select, values ) {
        for( n = 0; n < values.length; n++ ){
            var pair = values[n];
            select
                .append($("<option></option>")
                .attr("value",pair['id'])
                .text(pair['name']));
        }
    }


    var setSelectValue = function(element, value) {
        $(element).val(value).trigger("change");
    };
	
	$(function(){
		$("form").submit(function(){
			$("select").removeAttr('disabled');
		});
	});
  
    $("#territory-id,#town-id,#street-id").select2({
        width: "element",
        placeholder: COMBOBOX_LOADING,
        language: {
            inputTooLong:function(t){var n=t.input.length-t.maximum,r="Пожалуйста, введите на "+n+" символ";return r+=e(n,"","a","ов"),r+=" меньше",r},loadingMore:function(){return"Загрузка данных…"},
            maximumSelected:function(t){var n="Вы можете выбрать не более "+t.maximum+" элемент";return n+=e(t.maximum,"","a","ов"),n},
            inputTooShort: function () {
                return COMBOBOX_TOO_SHOT;
            },
            noResults: function () {
                return COMBOBOX_NOT_FOUND;
            },
            searching: function () {
                return COMBOBOX_LOADING;
            }
        },
        minimumResultsForSearch: 10
	});
	// Зануляем данные при первом открытии или возврате назад
	if ($('#town-id').val()==0) {
		$('#territory-id').val(0);
		$('#territory-id').change();
	}
});



