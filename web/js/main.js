function ShowLoadIndicator() {
	document.getElementById("load_indicator").style.display = "block";
}
function HideLoadIndicator() {
	document.getElementById("load_indicator").style.display = "none";
}
	
$( document ).ready(function() {
	var count = 0;		
     $( ".miss, .busy, .free, .liveLine" ).tooltip({
		tooltipClass: "tooltip-white-style",
		track: true,
  		position: { my: "center top+25", at: "right center" },
		content: function() {
			return $(this).attr('title');
		}
	});
	$( "body .info" ).tooltip({
		tooltipClass: "tooltip-white-style",
  		position: { my: "center-5 top+25", at: "right center" },
		content: function() {
			return $(this).attr('title');
		}
	});
	
	$( "body .biginfo" ).tooltip({
		tooltipClass: "tooltip-white-style toptooltip",
  		position: { my: "center bottom-5", at: "center top-10", collision:"none"},
		content: function() {
			return $(this).attr('title');
		}
	});
	
	$( "body .hiddentooltip" ).tooltip({
		tooltipClass: "tooltip-white-style",
  		position: { my: "center-35 top+25", at: "right center" },
		content: function() {
			return $(this).attr('title');
		}
	});
	$( ".dialog" ).dialog({
  		autoOpen: false,
  		modal: true
	});
	$( "a.trololo" ).click(function() {
		$(".popup.trololo").dialog({
			dialogClass: "Dialog activate",
			minWidth: 445
		})
	})
	$( "a.regNumber" ).click(function() {
		$(".popup.regNumber").dialog({
			dialogClass: "Dialog regnumber-win",
			minWidth: 445
		})
	})
	$( "a.regAgree" ).click(function() {
		$(".popup.regAgree").dialog({
			dialogClass: "Dialog",
			minWidth: 570
		})
	})
	$( "a.regNumber2" ).click(function() {
		$(".popup.regNumber2").dialog({
			dialogClass: "Dialog regnumber2-win",
			minWidth: 445
		})
	})
	$( "a.typepolis" ).click(function() {
		$(".popup.typepoliswin").dialog({
			dialogClass: "Dialog typepolis",
			minWidth: 600,
			height: document.body.clientHeight
		})
	})
	// Демобокс
	$(".demo").hover(function() {
		$(".demoBox").show();
	},
	function() {
		setTimeout(function() {
			if(!$(".demoBox").is(':hover')) $(".demoBox").hide();
		}, 300)
	});
	$(".demoBox").hover(
		function(){},
		function() {$( this ).hide();}
	);
	// Регбокс
	$(".register").hover(function() {
		$(".regBox").show();
	},
	function() {
		setTimeout(function() {
			if ($(".regBox").is(':visible')) {
				if(!$(".regBox").is(':hover')) $(".regBox").hide();
			}
		}, 300)
	});
	$(".regBox").hover(
		function(){},
		function() {$( this ).hide();}
	);
	$(".enter").click(function() {
		if ($(this).attr('href') == '#') {
			var target = $('.cap .person');
			$(".enterBox").dialog({
				dialogClass: "Dialog Test",
				minWidth: 315,
				position: { my:"left-150 top+4", at: "left-90 top", of: target},
				resizable: false
			})
			$('#login_email').focus();
			return false;
		}
	})
	
	$(document).on("click", "span.close", function() {
		$(this).parent().dialog( "close" );
		return false;
	});
	
	$("div.region").click(function(e) {
		if ( $(".region ul").hasClass("closed") ) {
			$(".region ul").addClass("opened");
			$(".region ul").removeClass("closed");
			e.stopPropagation();
			return false; 
		}
	});
	
	
	$(document).mouseup(function (e) {
		var container = $(".region ul");
		if ( !container.has(e.target).length ) {
			$(".region ul").addClass("closed");
			$(".region ul").removeClass("opened");
		}

		var container = $(".enterBox");

		if (!container.is(e.target)
			&& container.has(e.target).length === 0)
		{
			if ( container.dialog( "instance" ) ) {
				container.dialog( "close" );
			}
		}
		
		var container = $(".moreMenu, .moreopts");
		if ( !container.is(e.target) ) {
			$(".moreMenu").removeClass("showed");
		}
		
	});
	
	$("ul.opened").hover(function(){}, function(){$(this).addClass('Trololo2')})

	$("a.more").hover(
		function(){
			$(this).next(".moreMenu").show();
			$(this).addClass("hovered");
			$(this).next(".moreMenu").hover(
				function(){
					$(this).css("display", "block")
					},
				function(){
					$(this).css("display", "none")
					$(this).prev().removeClass("hovered");
					});
		},
		function(){
			$(this).next(".moreMenu").hide();
		}
	);
	
	$( ".lock" ).tooltip({
		tooltipClass: "tooltip-black-style",
		position: { my: "center-5 top+15", at: "right center" },
		content: function() {
			return $(this).attr('title');
		}
	});
	
	$(".timeLine ul.type li").tooltip({
		tooltipClass: "tooltip-black-style",
		position: { my: "center-13 top+15", at: "right center" },
		content: function() {
			return $(this).attr('title');
		}
	});
	
	$( "span.more" ).tooltip({
		tooltipClass: "tooltip-black-style thin-style",
		position: { my: "center-17 top+15", at: "right center", collision: "none" },
		content: "Подробнее",
		items: "span",
		show: { easing: "easeInExpo", duration: 800 }
	});
	
	$( "span.more" ).click(function(e, x){
		var nxt = $(this).next(".hidden"),
			ttp = $( "span.more" );
		if(nxt.is(":visible")) {
			ttp.tooltip("option", "content", MORE);
		} else {
			ttp.tooltip("option", "content", COLLAPSE);
		}
		nxt.slideToggle(0);
	});
	
	$( "a.lpuAddressTTLink" ).tooltip({
		tooltipClass: "tooltip-white-style lpuAddressTooltip",
		position: { my: "center+5 top+15", at: "center center" },
		content: function() {
			return $(this).next("span.lpuAddressHidden").html();
		},
		items: "a",
		show: {duration: 300 }
	});
	
	$('form input[type="password"] + label').click(function(e, x){
		var inputfield = $(this).prev("input");
		inputfield.toggleClass('opened');
		if(inputfield.attr('type')=="password")
		{inputfield.attr('type', 'text');}
		else{inputfield.attr('type', 'password');}
		return false;
	});
	
	$(window).on('beforeunload', function(){
        setTimeout(function() {
			ShowLoadIndicator();
		}, 1000)
	});
	
	$( document ).ajaxStart(function() {
	  tm = setTimeout(function() {
			ShowLoadIndicator();
		}, 1000)
	});
	$( document ).ajaxStop(function() {
	  clearTimeout(tm);
	  HideLoadIndicator();
	});
	$( document ).ajaxError(function(e, jqXHR, ajaxSettings, thrownError) {
		clearTimeout(tm);
		HideLoadIndicator();
		if (thrownError != 'abort') {
			$.alert(SERVER_ERROR);
		}
	});
	
	$.extend({ alert: function (message, title) {
		$(".popup.infoWindow>h2").html(title || ERROR);
		$(".popup.infoWindow>h2").removeClass();
		$(".popup.infoWindow>h2").addClass('alert');
		$(".popup.infoWindow>p.contextAlert").html(message);
		$(".popup.infoWindow").dialog({
			dialogClass: "Dialog infoWindow",
			minWidth: 445,
			modal: true
		})
	}
	});
	
	$(".forgot_pass_link").click(function() {
		if ( $("#request_password_div").length == 0 ) {
			$(document.body).append('<div id="request_password_div"></div>');
			$("#request_password_div").load( "/user/request_password_form", function() {
				var phone_mask = '+7 (999) 999-99-99';
				$('.telephonemask').mask(phone_mask);
				
				if ( $(".enterBox").hasClass('ui-dialog-content') ) {
					$(".enterBox").dialog( "close" );
				}
				var target = $('.cap .person');
				$(".recoveryPassMain").dialog({
					dialogClass: "Dialog Test",
					minWidth: 550,
					position: { my:"right+167 top+4", at: "left-90 top", of: target},
					resizable: false,
					open: function( event, ui ) {
						$('#request_password_mail').val($('#login_email').val());
					}
				})
			});
		} else {
			$(".enterBox").dialog( "close" );
			var target = $('.cap .person');
			$(".recoveryPassMain").dialog({
				dialogClass: "Dialog Test",
				minWidth: 450,
				position: { my:"right+167 top+4", at: "left-90 top", of: target},
				resizable: false,
				open: function( event, ui ) {
					$('#request_password_mail').val($('#login_email').val());
				}
			})
		}
		return false;
	});
	
	$( ".clientLpuAttachmentsTab span" ).click (function(e, x){
		var prt = $(this).parents(".docRecordingBlock ");
		prt.toggleClass('expandedAttachments');
	});
	
	$.extend({ info: function (message, title, type) {
		$(".popup.infoWindow>h2").html(title);
		$(".popup.infoWindow>h2").removeClass();
		if ( type ) {
			$(".popup.infoWindow>h2").addClass(type);
		}
		$(".popup.infoWindow>p.contextAlert").html(message);
		$(".popup.infoWindow").dialog({
			dialogClass: "Dialog infoWindow",/*Dialog добавляем всем попапам. Второй класс для специфики.*/
			minWidth: 445,
			modal: true
		})
	}
	});
	
});